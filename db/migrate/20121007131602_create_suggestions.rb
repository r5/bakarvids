class CreateSuggestions < ActiveRecord::Migration
  def change
    create_table :suggestions do |t|
      t.string :url
      t.string :email
      t.string :name

      t.timestamps
    end
  end
end
