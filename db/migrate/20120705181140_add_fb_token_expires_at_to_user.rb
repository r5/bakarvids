class AddFbTokenExpiresAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :FB_token_expires_at, :datetime
  end
end
