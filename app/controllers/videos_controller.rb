class VideosController < ApplicationController

  #before_filter :login_required, :only => [:show, :toggle_social]
  before_filter :find_video, :only => [:show]
  before_filter :sharing_mode_check, :only => [:delete_fb_post, :update_fb_post, :post_on_fb]


  def index
  end

  def search
    @videos = []
    if params[:name]
      @videos = Video.page(params[:page].to_i).per(12).where("caption like ?", '%' + params[:name] + '%').order("created_at DESC")
    end
    render("index")
  end

  def list
    if request.xhr?
     @videos = Video.page(params[:page].to_i).per(12).order("created_at DESC")
    end
    if @videos.present? 
      render("index", :layout => false) 
    else 
      render :text => "end"
    end
  end

  def contact
    Contact.create(:email => params[:email], :name => params[:name], :message => params[:message])
    redirect_to :back
rescue ActionController::RedirectBackError
  redirect_to root_path
  end

  def suggest
    Suggestion.create(:url => params[:url], :email => current_user ? current_user.email : "", :name => current_user ? current_user.first_name : "")
    redirect_to :back
rescue ActionController::RedirectBackError
  redirect_to root_path
  end

  def subscribe
    Subscription.create(:email => params[:email], :name => current_user ? current_user.first_name : "")
    redirect_to :back
rescue ActionController::RedirectBackError
  redirect_to root_path
  end
  

  def show
    @video.update_view_count
    render "show", :layout => "page"
  end

  def toggle_social
    current_user.sharing_mode = !current_user.sharing_mode
    @toggled  = current_user.save
  end

  def post_on_fb
    video = Video.where(:id => params[:vid]).first
    if video
      res = HTTParty.post("https://graph.facebook.com/me/video.watches?video=#{video.slug_url}&access_token=#{current_user.access_token}&expires_in=#{params[:expires_in]}")
     p video.slug_url
      p res
       session[:bakavids_plaball] = {:fb_id => res.parsed_response["id"], :vcap => video.caption} if(res.response.code == "200" && res.parsed_response["id"])
    end
    res.response.code == "200" ? render(text: "ok", status: 200) : render(text: "bad", status: 404)
  end
  
  def delete_fb_post
    begin
     p session
    watch_id = session[:bakavids_plaball][:fb_id]
    res = HTTParty.delete("https://graph.facebook.com/#{watch_id}?access_token=#{current_user.access_token}")
    p res
    rescue
    end
    session.delete(:bakavids_plaball)
    render(text: "OK", status: 200)
  end

  def update_fb_post
    begin
    watch_id = session[:bakavids_plaball][:fb_id]
    res = HTTParty.post("https://graph.facebook.com/#{watch_id}?access_token=#{current_user.access_token}&expires_in=#{params[:expires_in]}")
    p res
    rescue
    end
    session.delete(:bakavids_plaball)
    render(text: "OK", status: 200)
  end

  private
    def find_video
      @video = Video.find_by_slug(params[:caption]) if params[:caption]
      redirect_to videos_path, :error => "Sorry, this video could not be found!" unless @video
    end

    def login_required
      unless current_user
        redirect_to root_path, :notice => "Please sign in with Facebook!"
      end
    end

    def sharing_mode_check
      unless current_user.sharing_mode
         render(text: "OK", status: 200) and return
      end
    end
end
