class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    # You need to implement the method below in your model
    puts "FB Data: #{request.env["omniauth.auth"]}" 
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    logger.debug "FB Data: #{request.env["omniauth.auth"]}"
    if @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in @user
      redirect_to root_path
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      flash[:alert] = "There was an error while logging in!"
      redirect_to root_path
    end
  end

  def failure
    redirect_to root_path
  end
end