class UserToken < ActiveRecord::Base
  attr_accessible :acess_token, :first_name, :last_name, :location, :profile_url, :social_email, :uid
end
