class Video < ActiveRecord::Base
  attr_accessible :caption, :url, :image, :description, :image_url
  has_attached_file :image, :styles => { :lofer => "200x240#", :romio => "200x310#" ,:play => "200x276#", :thumb => "222x175#" }
  validates :caption, :uniqueness => true

  def slug_path
  	"/video/#{self.caption.gsub(" ", "-")}"
  end

  def slug_url
  	"http://bakarvids.com" + "/video/#{self.caption.gsub(" ", "-")}"
  end

  def self.find_by_slug(caption)
  	Video.where(:caption => caption.gsub("-", " ")).first
  end

  def update_view_count
  	self.update_attribute('view_count', self.view_count + 1)
  end
end
