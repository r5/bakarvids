class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and 
  devise :trackable, :omniauthable,:database_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password,:profile_url, :password_confirmation,:sharing_mode, :profile_image, :remember_me,:uid, :first_name, :last_name, :prfile_url, :location,:access_token, :FB_token_expires_at
  # attr_accessible :title, :body


   #this method is for finding or creating a user after we get a callback from facebook
  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    info = access_token.info
    if user = User.where(:uid => access_token.uid.to_s).first
      user.access_token = access_token.credentials.token
      user.save
      user
    else # Create a user with a stub password, we will change it right after
      user = User.new(
        :email => data.email || access_token.uid + "@facebook.com",
        :password => Devise.friendly_token[0,20],
        :first_name => data.first_name,
        :last_name => data.last_name,
        :profile_url => info.urls.Facebook,
        :location => info.location,
        :FB_token_expires_at => Time.zone.at(access_token.credentials.expires_at),
        :uid => access_token.uid,
        :access_token => access_token.credentials.token,
        :profile_image => info.image,
        :sharing_mode => true
        ) 
      user.save
      user
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"]
      end
    end
  end
  
  def update_with_password(params={}, userid, fbtime)
      created_date = User.find(userid).created_at
      if fbtime
        real = !((fbtime.to_time - 5.minutes) > created_date)
      end
      current_password = params.delete(:current_password)

      if params[:password].blank?
        params.delete(:password)
      end 

      result = if !params[:password] || valid_password?(current_password) || real
        update_attributes(params)
      else
        self.attributes = params
        self.valid?
        self.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
        false
      end 

      clean_up_passwords
      result
    end
end
