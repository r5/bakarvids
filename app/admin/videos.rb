ActiveAdmin.register Video do

	form do |f|
      f.inputs "Details" do
        f.input :caption
        f.input :url
        f.input :description
        f.input :image_url
      end
      f.buttons
    end

   	show do |video|
      attributes_table do
        row :caption
        row :url
        row :description
        row :image do
          image_tag(video.image.url)
        end
      end
      active_admin_comments
    end
  
end
