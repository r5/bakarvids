//= require froogaloop

var Vimeo = function(config) {
    this.player = Froogaloop(document.getElementById('vplayer'));
}

function fun(s){
    alert(s)
}

Vimeo.prototype = {
    registerEvents: function(){
        var that = this;
        that.player.addEvent('ready', function() {            
            that.player.addEvent('play', that.playerState);
            that.player.addEvent('pause', that.playerState);
        });

        $(window).bind('beforeunload', function() {
            if($(".on-off").hasClass("main-on"))
            vimeo.playerState(2) ;
        });
    },
    actionAfterTimeCalculation: function(action){
        this.getDuration()
    },
    getDuration: function(){
        window.duration;
        this.player.api('getDuration', function(t){ 
            window.duration = t;
            vimeo.getCurrentTime();
        });
    },
    getCurrentTime: function(){
        var that = this;
        window.currentTime;
        this.player.api('getCurrentTime', function(t){
            window.currentTime = t;
            if(vimeo.currentAction){
                vimeo.currentAction();
            }
            else{
                if( (window.currentTime * 2) > window.duration){
                    vimeo.updateFbPost();
                }
                else{
                    vimeo.deletePostOnFb();
                    $.cookie("postfb", "false");
                }
            }
        });

    },
    fun: function(s){
        alert(s)
    },
    playerState: function(state){  
        var that = vimeo;
        if($(".on-off").hasClass("main-on")){
            if(state == 1 && (!$.cookie("postfb") || $.cookie("postfb") == "false")){
                that.currentAction = that.postOnFb;
                that.actionAfterTimeCalculation();
                $.cookie("postfb", "true");
            }
            else if(state == 2){
                window.currentTime = null;
                window.duration = null;
                that.currentAction = null;
                that.actionAfterTimeCalculation();
                
            }
        }
    },
    postOnFb: function(){
        var that = this;
        this.sendRequestToAction({url: "/videos/postfb", type: "POST", data: {expires_in: (window.duration - window.currentTime) ,vid: $("#vbox").data('video_id')}});
    },
    deletePostOnFb: function(){
        var that = this;
        this.sendRequestToAction({url: "/videos/delete_fb_post", type: "POST", data: null});
    },
    updateFbPost: function(){
        var that = this;
        this.sendRequestToAction({url: "/videos/update_fb_post", type: "POST", data: {expires_in: (window.duration - window.currentTime) } });
    },
    sendRequestToAction: function(config) {
        $.ajax({
            url: config.url,
            data: config.data,
            type: config.type,  
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content') );
            },
            success: function() {
            }
        });
    }
}


var vimeo;
jQuery(document).ready(function() {
    vimeo =  new Vimeo({});
    $.cookie("postfb", "false");
    vimeo.registerEvents();


});

  
// jQuery(document).ready(function() {
 	
//  	// Enable the API on each Vimeo video
//     jQuery('iframe#vplayer').each(function(){
//         Froogaloop(this).addEvent('ready', ready);
//     });
    
//     function ready(playerID){
//         // Add event listerns
//         // http://vimeo.com/api/docs/player-js#events
//         Froogaloop(playerID).addEvent('play', play(playerID));
//         Froogaloop(playerID).addEvent('seek', seek);
//         debugger
        
//         // Fire an API method
//         // http://vimeo.com/api/docs/player-js#reference
//         Froogaloop(playerID).api('play');
//     }
//     function play(playerID){
//         alert(playerID + " is playing!!!");
//     }
//     function seek() {
//         alert('Seeking');
//     }

// });