//= require isotope



function csscsstransitions(){
	var animation = false,
     animationstring = 'animation',
     keyframeprefix = '',
     domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
     pfx  = '';
     elm = $("#top-container-bakar")[0];
 
	if( elm.style.animationName ) { animation = true; }    
	 
	if( animation === false ) {
	  for( var i = 0; i < domPrefixes.length; i++ ) {
	    if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
	      pfx = domPrefixes[ i ];
	      animationstring = pfx + 'Animation';
	      keyframeprefix = '-' + pfx.toLowerCase() + '-';
	      animation = true;
	      break;
	    }
	  }
	}
	return animation;
}



var DealBoard = function(){
	this.container = $("#iso-container");
	this.newRequest = true;
	this.profileInfo = null;
	this.page = 2;
	this.topIndex = 100;
	this.disableHashChange = false;
	this.disableScrolling = false;
	this.boardIsopeSetupOptions = 	{
		masonry: {
			columnWidth: 160
		},
		itemSelector : '.bakar-item',
		resizeable: false
	};
	this._create();	
}

DealBoard.prototype = {
	loadResultsForURL: function(config){
    // Changing /people/index to /people .. Not getting used as of now .. just a dummy value
    // all the urls set based on window.location peoperties
		var defaults = {
			url: "", 
			data: {},
			success: function(){},
			complete: function(){},
			error: function(){},
			beforeSend: function(){},
			afterInsert: function($data){},
			beforeInsert: function($data){},
			removeCurrentElements: true
		}
		config = $.extend({}, defaults, config )
		var that = this;
		$.ajax({
			url: config.url,
			data: config.data,	
			beforeSend: function(){
				config.beforeSend();
			},
			success: function(data, textStatus, jqXHR){
				$data = $(data);
				if(config.removeCurrentElements)
					that.container.isotope("remove", $(".element"));
				else
				// that.shuffle($data)
				// that.assignRandonIndex($data);
				config.beforeInsert($data);
				that.container.isotope( 'insert', $data, function(){
					config.afterInsert($data);
				});
				config.success(data);
			},
			error: function(jqXHR, textStatus, errorThrown){
		        if(jqXHR.readyState == 0 || jqXHR.status == 0) {
		          return;  // it's not really an error
		        }
		        else {
		          alert("Could not retrieve the profile. Please try again.");
		        }
				config.error();
			},
			complete: function(){
				config.complete();	
			}
		});

	},
	_create: function(){
		var that = this;
		that.container.isotope(that.boardIsopeSetupOptions);
		that.scrollable();
		that.registerEvents();
		if(!location.href.match('search'))
			that.loadResultsForURL({url: "/videos/page/1"})
	},
	scrollable: function(){
		window.scrolling = false
		var that = this;
		$(window).bind("scroll", function() {
			if(window.scrolling) return;
			if($(window).scrollTop() + $(window).height() + 150 >= $(document).height()) {
				window.scrolling = true;
				if(!that.disableScrolling){
					var anchor = $('a#will-paginate');
					that.loadResultsForURL({
						removeCurrentElements: false,
						url: "/videos/page/" + that.page,
						beforeInsert: function(newElements){
						},
						success: function(data){
							if(data == "end")
							{
								that.disableScrolling = true
							}
							that.page++

						},
						afterInsert: function(){
							window.scrolling = false;
						},
						error: function(){
							window.scrolling = false;
						}
					});
				}
			}
		});
  	},
	registerEvents: function(){
		var that = this;

		
	}

}



var dealBoard = "";
$(function(){
	dealBoard = new DealBoard();
	setTimeout("dealBoard.container.isotope('reLayout')", 800);
	
});
