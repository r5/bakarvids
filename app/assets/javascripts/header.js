$(function() {

   var switchToggle = function(callbacks){
    var on = false;
    return function(){
      on = $(this).hasClass("main-on");
      if (on){
        $(this).find('.switch').stop().animate({left: 0}, 400);
        $(this).find('.on span').stop().delay(70).animate({right: '100%'}, 350);
        $(this).removeClass("main-on");
      } else {
        $(this).find('.on span').stop().animate({right: 0}, 400);
        $(this).find('.switch').stop().delay(100).animate({left: $(this).width() - $('.switch').outerWidth()}, 300);
        $(this).addClass("main-on");
      }
      on = !on;
      $('input', this).prop('checked', on)

      if (on && callbacks && callbacks.on)
        callbacks.on.apply(this);
      else if (!on && callbacks && callbacks.off) 
        callbacks.off.apply(this);

      if (callbacks && callbacks.change)
        callbacks.change.apply(this);
    };
  }

   $('.on-off').click(switchToggle({
    change: function(){
      $.ajax({
        url: "/videos/toggle_social",
        type: "POST"
      });
    }
  }));

  function getScrollTop() {
    if (typeof window.pageYOffset !== 'undefined' ) {
      // Most browsers
      return window.pageYOffset;
    }

    var d = document.documentElement;
    if (d.clientHeight) {
      // IE in standards mode
      return d.scrollTop;
    }

    // IE in quirks mode
    return document.body.scrollTop;
  }

  window.onscroll = function() {
    var box = $('#subnav-bakar'),
        scroll = getScrollTop();
    if (scroll >= 50) {
      if(!box.hasClass('navbar-fixed-top')) box.addClass('navbar-fixed-top');
    }
    else {
      if(box.hasClass('navbar-fixed-top')) box.removeClass('navbar-fixed-top');
    }
  };

});