var YouTube = function(config) {
	this.player = document.getElementById('bakarplayer');
}

YouTube.prototype = {
	registerEvents: function(){
		this.player.addEventListener('onStateChange', 'youtube.playerState');
		$(window).bind('beforeunload', function() {
			if($(".on-off").hasClass("main-on")){
      	youtube.playerState(2);
    	}
		});
	},
	playerState: function(state){
		var that = this;
		if($(".on-off").hasClass("main-on")){
			if(state == 1 && (!$.cookie("postfb") || $.cookie("postfb") == "false")){
					that.postOnFb();
				$.cookie("postfb", "true");
			}
			else if(state == 2){
				if( (that.player.getCurrentTime() * 2) > that.player.getDuration()){
					that.updateFbPost();
				}
				else{
					that.deletePostOnFb();
					$.cookie("postfb", "false");
				}
			}
		}	
	},
	postOnFb: function(){
		var that = this;
		this.sendRequestToAction({url: "/videos/postfb", type: "POST", data: {expires_in: (that.player.getDuration() - that.player.getCurrentTime()) ,vid: $("#vbox").data('video_id')}});
	},
	deletePostOnFb: function(){
		var that = this;
		this.sendRequestToAction({url: "/videos/delete_fb_post", type: "POST", data: null});
	},
	updateFbPost: function(){
		var that = this;
		this.sendRequestToAction({url: "/videos/update_fb_post", type: "POST", data: {expires_in: (that.player.getDuration() - that.player.getCurrentTime()) } });
	},
	sendRequestToAction: function(config) {
		$.ajax({
			url: config.url,
			data: config.data,
			type: config.type,	
			beforeSend: function(xhr){
				xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content') );
			},
			success: function() {
			}
		});
	}
}

var youtube;
function onYouTubePlayerReady(){
	youtube =  new YouTube({});
	$.cookie("postfb", "false");
	youtube.registerEvents();
}

