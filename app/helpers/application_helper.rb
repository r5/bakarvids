module ApplicationHelper

	def spacer(height)
		"<div class='spcer' style='height:#{height}'></div>".html_safe
	end
end
