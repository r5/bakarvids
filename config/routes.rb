Bakarvids::Application.routes.draw do
  devise_for :users, :controllers => {:omniauth_callbacks => "users/omniauth_callbacks" }

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  
  get "video/:caption", to: 'videos#show'
  post "videos/delete_fb_post", to: "videos#delete_fb_post"
  post "videos/update_fb_post", to: "videos#update_fb_post"
  match 'auth/:provider/callback', to: 'videos#twitter'
  post '/suggest_video', to: 'videos#suggest'
  post '/subscribe', to: 'videos#subscribe'
  match '/search', to: 'videos#search'
  match '/contact', to: 'videos#contact'
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  match "privacy", to: "static#privacy"
  match "tos", to: "static#tos"

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products
  resources :videos do
    collection do
      post 'toggle_social'
    end
  end
  match "videos/page/:page", to: 'videos#list'
  match "videos/postfb", to: 'videos#post_on_fb', as: :post_on_fb
  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'videos#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
